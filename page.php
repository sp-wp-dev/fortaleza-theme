<?php get_header(); ?>
    <div class="bf-page">
        <div class="container-fluid mb-4">
            <div class="row mt-2 ml-2">
                <?php
                    $array_pages=Array("Area Legal","Auditoria","División Finanzas","División Negocios","División Operaciones","División Riesgos","Talento Humano","Tecnología","Memorias");
                    $array_slug=Array("legal","auditoria","finanzas","negocios","operaciones","riesgos","talento-humano","tecnologia","memorias");
                    $post = $wp_query->get_queried_object();
                    $post_parent= get_post($post->post_parent);
                    $indice = array_search( $post->post_name,$array_slug,true);
                    $attachments = get_post_meta( $post->ID, '_da_attachments', true );
                    if ( is_array( $attachments ) && ! empty( $attachments )) {
                        get_template_part('template-parts/content');
                    }else{
                        if($post->post_parent == 0):
                            get_template_part('template-parts/content', 'areas');
                        else:
                            get_template_part('template-parts/content','areas-content');
                        endif;
                    }

                ?>  
            </div>
        </div>
    </div>
<?php get_footer();?>