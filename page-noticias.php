<?php get_header(); ?>

    <div class="container mb-4">
        <div class="row my-2">
estoy
            <!-- Entrada -->
            <div class="col-md-9 px-0">
                <?php
                    $news_loop = new WP_Query( array(
                    'post_type' => 'comunicados',
                        //  'posts_per_page' => 2 // put number of posts that you'd like to display
                    ) );
                ?>
                <?php if ( $news_loop->have_posts() ) : while ( $news_loop->have_posts()) :  $news_loop->the_post(); ?>
                    
                    <!-- Contenido -->
                    <div class="card my-2">
                        <div class="card-body">
                            <a class="bf-card-title" href="<?php the_permalink(); ?>">
                                <h5 class="card-title"><?php the_title(); ?> </h5>
                            </a>         
                            <div class="row">
                                <div class="col-md-6">
                                <?php
                                    $imagen = get_field('imagen_destacada');
                                    if($imagen){ ?>
                                        <a href="<?php echo get_permalink($post->ID);?>">
                                            <img src="<?php echo $imagen['url']; ?>" alt="<?php echo $imagen['alt']; ?>" style="width: 100%; height: 150px;"/>
                                            </a>
                                        <?php
                                    }	
                                ?>
                                </div>
                                <div class="col-md-6">
                                    <?php the_content(); ?>
                                </div>
                            </div>                  
                            <p class="card-text text-right mb-0"><strong>Fecha de publicación : </strong><?php the_time('F j, Y'); ?>  </p>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-3">
                <!-- Sidebar Derecha -->
                <?php get_sidebar('right'); ?>  
            </div>
        </div>
    </div>
<?php get_footer();?>