<?php get_header(); ?>

    <div class="container-fluid mb-4">
        <div class="row m-2">
            <?php
                $systems_loop = new WP_Query( array(
                'posts_per_page' => 5,
                'post_type' => 'sistemas',
                ) );
            ?>
            <!-- Entrada -->
            <div class="col-md-9 px-0">
                <?php if ( $systems_loop->have_posts() ) : while ( $systems_loop->have_posts() ) : $systems_loop->the_post(); ?>
                    
                    <!-- Contenido -->
                    <div class="card mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-3">
                                    <?php 
                                        if ( has_post_thumbnail() ) {
                                            the_post_thumbnail('post-thumbnails', array(
                                                'class' => 'img-fluid mb-3'
                                            ));
                                        }
                                    ?>
                                </div>
                                <div class="col-12 col-md-9">
                                    <div class="bf-card-title">
                                        <h5 class="card-title"><?php the_title(); ?> </h5>
                                    </div>                            
                                    <?php the_content(); ?>
                                    <div>
                                        <strong>URL : </strong>
                                        <a href="<?php echo get_field("enlace_del_sistema"); ?>" target="_blank" rel="noopener noreferrer"><?php echo get_field("enlace_del_sistema"); ?></a>
                                    </div>
                                    <p class="card-text text-right mb-0"><strong>Fecha de publicación : </strong><?php the_time('F j, Y'); ?>  </p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-3">
                <!-- Sidebar Derecha -->
                <?php get_sidebar('right'); ?>  
            </div>
        </div>
    </div>
<?php get_footer();?>