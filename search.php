<?php get_header(); ?>

    <div class="container-fluid bf-search mb-4">
        <div class="row m-2">

             <!-- Entrada -->
            <div class="col-md-9 px-0">
				<div class="row title-container mx-0">
					<div class="col">
						<h2 class="title-page">Resultados de búsqueda</h2>
					</div>
					<div id="breadcrumb" class="col text-right pr-2">
						<a class="btn-breadcrumb" href="<?php echo get_home_url() ?>" name="volver atrás"><i class="fa fa-home"></i> Inicio </a>
					</div>
				</div>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php if($post->post_type=='attachment'){
						$post_parent = get_post($post->post_parent);
						?>
						<!-- Contenido -->
						<div class="card">
							<div class="card-body">
								<div class="bf-card-title">
									<a href="<?php echo get_permalink($post_parent->ID); ?> ">
										<h5 class="card-title"><?php echo $post_parent->post_title; ?> </h5>
									</a>
								</div>      
								<div class="row content-search">
									<div class="col">
										<?php 
										$url_icon =get_home_url().'/wp-content/plugins/download-attachments/images/ext/';
										switch ($post->post_mime_type) {
											case 'application/pdf':
												?>
												<img class="attachment-icon" src="<?php echo $url_icon;?>pdf.gif" alt="pdf">
												<?php 
												break;
											case 'application/msword':
												?>
												<img class="attachment-icon" src="<?php echo $url_icon;?>doc.gif" alt="doc">
												<?php 
												break;
											case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
												?>
												<img class="attachment-icon" src="<?php echo $url_icon;?>doc.gif" alt="doc">
												<?php 
												break;
											case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
												?>
												<img class="attachment-icon" src="<?php echo $url_icon;?>xls.gif" alt="xls">
												<?php 
												break;
											case 'application/vnd.oasis.opendocument.spreadsheet':
												?>
												<img class="attachment-icon" src="<?php echo $url_icon;?>xls.gif" alt="xls">
												<?php 
												break;
											case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
												?>
												<img class="attachment-icon" src="<?php echo $url_icon;?>ppt.gif" alt="ppt">
												<?php 
												break;
											default:
												?>
												<img class="attachment-icon" src="<?php echo $url_icon;?>unknown.gif" alt="unknown">
												<?php 
												break;
										}
										 ?>
										 <span class="search-file">
											 <a target="_blank" href="<?php echo get_home_url().'/download/'.$post->ID?>"><?php echo $post->post_title ?></a>
										</span>
									</div>
									<?php 
									$category = get_category_parent_by_attachment($post);
									$category_content = get_category_container_by_attachment($post);
									if($post_parent->post_type=='page'){
										$post_parent_content= get_post($post->post_parent);
										$post_parent= get_page_parent_by_page($post_parent);
										?>
										<div class="col-3 text-capitalize">
											<div>
												<span><strong>Area : </strong> <?php echo esc_html( $post_parent->post_name); ?> </span>
											</div>
											<div>
												<span><strong>Sección : </strong> <?php echo esc_html( $post_parent_content->post_name ); ?> </span>
											</div>
										</div>
										<?php 
									}else{
									?>
									<div class="col-3 text-capitalize">
										<div>
											<span><strong>Area : </strong> <?php echo esc_html( $category->name ); ?> </span>
										</div>
										<div>
											<span><strong>Sección : </strong> <?php echo esc_html( $category_content->name ); ?> </span>
										</div>
									</div>
									<?php 
									}
									?>
								</div>                      
							</div>
						</div>
						<?php 
					}
					else{
						$category = get_category_parent_by_post($post);
					?>
					<!-- Contenido -->
                    <div class="card">
                        <div class="card-body">
                            <div class="bf-card-title">
								<a href="<?php the_permalink(); ?> ">
									<h5 class="card-title"><?php the_title(); ?> </h5>
								</a>
                            </div>      
							<div class="row content-search">
								<div class="col">
								<?php the_content(); ?>
								</div>
								<div class="col-3 text-capitalize">
									<div>
										<span><strong>Area : </strong> <?php echo esc_html( $category->name ); ?> </span>
									</div>
									<div>
										<span><strong>Sección : </strong> <?php echo esc_html( $category_content->name ); ?> </span>
									</div>
								</div>
							</div>                      
                        </div>
                    </div>
					<?php 		
					}
					?>
                    
                <?php endwhile;
				else: ?>
					<div class="row">
						<div class="col-12">
							<h5>No se encontró el termino: 
								<?php printf(esc_html('%s'),get_search_query());?>
							</h5>
							<p>Lo sentimos, pero nada coincide con sus criterios de búsqueda. Vuelva a intentarlo con otras palabras diferentes.</p>
						</div>
						<div class="col-6">
							<?php get_search_form(); ?>
						</div>
					</div>
					<?php  ?>
				<?php endif; ?>

				<!-- Paginación -->
				<?php echo bootstrap_pagination(); ?> 

            </div>
            <!-- Sidebar Derecha -->
            <div class="col-md-3">
                <!-- Sidebar Derecha -->
                <?php get_sidebar('page'); ?>  
            </div>
        </div>
    </div>
<?php get_footer();?>