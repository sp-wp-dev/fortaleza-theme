<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Fortaleza
 * @since Fortaleza 1.0
 * @version 1.0
 */

?>
<?php $background_image = get_theme_support( 'custom-header', 'default-image' );
if ( has_header_image() ) {
	$background_image = get_header_image();
} ?>

<div class="bf-nav-widget-area-back">
	<img src="<?php echo esc_url( $background_image ); ?>" class="img-fluid" alt="">
	<div class="overlay">
		<div class="inner"> 
			<div class="container-fluid">
				<div class="row justify-content-between">
					<div class="col-lg-3 col-md-5">
						<div class="bf-nav-widget-area">
							<div class="row align-items-center">
								<div class="col text-center-xs">
									<div class="navbar-header">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php the_custom_logo(); ?> </a>
										<?php  
										if (display_header_text()) : ?>
											<div class="site-branding-text">
												<h1 class="site-title"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
												<p class="site-description"><?php bloginfo('description'); ?></p>
											</div>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="notice">
						<?php
							$new_loop = new WP_Query( array(
							'post_type' => 'comunicados',
								//  'posts_per_page' => 2 // put number of posts that you'd like to display
							) );
						?>
						<div id="carouselBf" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<?php $count = 0; ?>
								<?php if ( $new_loop->have_posts() ) : ?>
									<?php while ( $new_loop->have_posts() ) : $new_loop->the_post();
										date_default_timezone_set('America/La_Paz');
										$fechaActual = date("d/m/Y");
										$dateTimeI = DateTime::createFromFormat('Ymd', get_post_meta($post->ID, 'fecha_inicio', TRUE));
										$dateTimeFin = DateTime::createFromFormat('Ymd', get_post_meta($post->ID, 'fecha_fin', TRUE));
										$fechaI =date_format($dateTimeI, "d/m/Y");
										$fechaFin = date_format($dateTimeFin, "d/m/Y");
										if(($fechaI >= $fechaActual && $fechaActual <= $fechaFin)||($fechaI <= $fechaActual && $fechaFin>=$fechaActual)):?>
											<div class="carousel-item <?php echo ($count == 0) ? 'active' : ''; ?>">
												<?php
													$imagen = esc_url(get_the_post_thumbnail_url(get_the_ID()));
													if($imagen){ ?>
														<h3 id="badge-new" class="badge-new"><span class="badge badge-danger">Nuevo</span></h3>
														<a href="<?php echo get_permalink($post->ID);?>">
															<img src="<?php echo $imagen; ?>" alt="<?php echo $imagen; ?>" style="width: 100%; height: 300px;"/>
															</a>
														<?php
													}	
												?>
											</div>
											<?php $count++; ?>
										<?php endif;?>
									<?php endwhile;?>
								<?php else: ?>
								<?php endif; ?>
								<?php wp_reset_query(); ?>
							</div>
							<!-- <a class="carousel-control-prev" href="#carouselBf" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselBf" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a> -->
						</div>

					</div>	
				</div>
			</div>  
		</div>

	</div> 
</div>

