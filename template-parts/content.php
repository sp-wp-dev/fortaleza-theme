<!-- Entrada -->
<div class="col-md-9 px-0">
    <div class="row title-container mx-0">
        <div class="col">
            <h2 class="title-page">
                <?php single_post_title(); ?>
            </h2>
        </div>
        <div id="breadcrumb" class="col text-right pr-2">
                    <?php bf_breadcrumbs(); ?>
        </div>
    </div>
    <?php if ( have_posts() ) : while (have_posts() ) : the_post(); ?>
        <div class="card mb-2">
            <div class="card-body">
                <div class="bf-card-title">
                    <h5 class="card-title"><?php the_title(); ?> </h5>
                </div>                            
                <?php 
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail('post-thumbnails', array(
                            'class' => 'img-fluid mb-3'
                        ));
                    }
                ?>
                <?php the_content(); ?>
            </div>
        </div>
    <?php endwhile;
    else : ?>
        <div class="bf-card-title">
            <h5 class="card-title"> No existe contenido </h5>
        </div>    
    <?php
    endif; wp_reset_postdata(); ?>
    <?php echo bootstrap_pagination(); ?> 
</div>
<div class="col-md-3">
    <?php get_sidebar('page'); ?>  
</div>