<?php 
    $post_parent= get_post($post->post_parent);
    $post_name = $post->post_name;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $depth = 1;
    while ($post_parent->post_parent !== 0){
        $slug_category2.='-'.$post_parent->post_name;
        $post_parent= get_post($post_parent->post_parent);
        $depth++;
    }
    $slug_category2.='-'.$post_parent->post_name;
    $slug_category=$post_name.$slug_category2;
    $page_loop = new WP_Query( array(
    'paged'=> $paged,
    'category_name' => $slug_category,
    ) );
?>
<div class="col-md-9 px-0">
    <div class="row title-container mx-0">
        <div class="col">
            <h2 class="title-page">
                <?php single_post_title(); ?>
            </h2>
        </div>
        <div id="breadcrumb" class="col text-right pr-2">
                    <?php bf_breadcrumbs(); ?>
        </div>
    </div>
    <?php if ( $page_loop->have_posts() ) : while ( $page_loop->have_posts() ) : $page_loop->the_post(); ?>
    <?php 
    $categories = get_the_category();
    $key= array_search($slug_category, array_column($categories, 'slug'));
    if($key !== false){
     ?>
        <div class="card mb-2">
            <div class="card-body">
                <div class="bf-card-title">
                    <h5 class="card-title"><?php the_title(); ?> </h5>
                </div>                            
                <?php 
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail('post-thumbnails', array(
                            'class' => 'img-fluid mb-3'
                        ));
                    }
                ?>
                <?php the_content(); ?>
            </div>
        </div>
    <?php
    }
    endwhile; 
    else : ?>
        <div class="bf-card-title">
            <h5 class="card-title"> No existe contenido </h5>
        </div>    
    <?php
    endif;  wp_reset_postdata();  ?>
    <!-- Paginación -->
    <?php 
    if($key !== false){
        echo bootstrap_pagination($page_loop);
    }
    ?> 
</div>
<div class="col-md-3">
    <?php get_sidebar('page'); ?>  
</div>
