<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Fortaleza
 * @since Fortaleza 1.0
 * @version 1.0
 */

?>
<section class="container-fluid">
    <div class="row my-2">
        <div class="col-md-6 col-sm-12"">
            <?php
                $video_loop = new WP_Query( array(
                'posts_per_page' => 5,
                'post_type' => 'post',
                'category_name'=>'video'
                ) );
            ?>
            <div class="title-wrap">
                <div class="row">
                    <div class="col-9">
                        <a class="featured-title" href=" <?php echo get_permalink( get_page_by_path( 'galeria' ) ); ?>">Nuestros Videos</a>
                    </div>
                    <div class="col-3 text-right">
                        <a class="featured-button" href="#carouselVideoBf" role="button" data-slide="prev">
                           <i class="fa fa-angle-double-left"></i>
                        </a> 
                        <a class="featured-button" href="#carouselVideoBf" role="button" data-slide="next">
                           <i class="fa fa-angle-double-right"></i>
                        </a> 
                    </div>
                </div>
            </div>
            <div class="row wrap-video">
                <div class="col-12">
                    <div id="carouselVideoBf" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" >
                            <?php $count = 0; ?>
                            <?php if ( $video_loop->have_posts() ) : ?>
                                <?php while ( $video_loop->have_posts() ) : $video_loop->the_post();?>
                                <div class="carousel-item <?php echo ($count == 0) ? 'active' : ''; ?>">
                                     <?php the_content(); ?>
                                </div>
                                <?php $count++; ?>
                                <?php endwhile;?>
                            <?php else: ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 ">
            <?php
             $ins_args = array(
                'post_type' =>  array('post','attachment'),
                'posts_per_page' => absint(5),
                'post_status' => array('publish','inherit'),
                'orderby' => 'date',
                'order' => 'DESC',
                'ignore_sticky_posts' => true
            );
            $news_loop = new WP_Query($ins_args);
            ?>
            <div class="title-wrap">
                <div class="row">
                    <div class="col-9">
                        <h3 class="featured-title">
                            Noticias
                        </h3>
                    </div>
                </div>
            </div>
            <div class="news-widget row">
                <div class="col-12">
                    <?php if ( $news_loop->have_posts() ) : ?>
                        <?php while ( $news_loop->have_posts() ) : $news_loop->the_post();?>
                        <div class="row mb-2">
                            <div class="col-md-4 text-center">
                                <?php
                                    $imagen = esc_url(get_the_post_thumbnail_url(get_the_ID()));
                                    if($imagen): ?>
                                        <a href="<?php echo get_permalink($post->ID);?>">
                                            <img class="img-fluid" src="<?php echo $imagen; ?>" alt="<?php echo $imagen; ?>" style="height: 75px;"/>
                                        </a>
                                        <?php
                                    else:?>
                                        <a href="<?php echo get_permalink($post->ID);?>">
                                            <img class="img-fluid" src="<?php echo get_parent_theme_file_uri( '/assets/images/fortaleza.jpg' );?>" alt="<?php echo $imagen; ?>"/>
                                        </a>
                                        <?php
                                    endif;	
                                ?>
                            </div>
                            <div class="col-md-8 pl-0">
                                <div class="row">
                                    <div class="col">
                                        <h6 class="news-title-text text-capitalize text-truncate"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6> 
                                    </div>
                                    
                                </div>
                                <div class="row justify-content-between">
                                    <div class="col-xl-7 col-lg-12 text-left" >
                                        <?php
                                        if($post->post_type ==='attachment'){
                                            $post_parent = get_post($post->post_parent);
                                            $categories = wp_get_post_categories($post_parent->ID);
                                            if(count($categories)==0){
                                                $page_parent = wp_get_post_parent_id($post_parent->ID);
                                            }
                                        }else{
                                            $categories =wp_get_post_categories($post->ID);
                                        }
                                        $category = get_category($categories[0]);
                                        $category_parent_id = $category->category_parent;
                                        if ( ! empty( $categories ) ):?>
                                        <strong>Sección : </strong> 
                                        <a class="text-capitalize" href="<?php echo esc_url( get_category_link( $category->term_id ) ); ?>">
                                            <span class="badge badge-primary"><?php echo esc_html( $category->name );?> </span>
                                        </a>   
                                        <?php endif;
                                        $category = get_category($category_parent_id);
                                        while($category->category_parent!=0){
                                            $category = get_category($category->category_parent);
                                        }
                                        ?>    
                                        <div>
                                            <span><strong>Area : </strong> <?php echo esc_html( $category->name ); ?> </span>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-lg-12 text-right text-capitalize">
                                        <p class="news-content-text"><i class="fa fa-clock-o"></i> <?php the_time('F j, Y'); ?>  </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile;?>
                    <?php else: ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 ">
            <?php
                $systems_loop = new WP_Query( array(
                'posts_per_page' => 5,
                'post_type' => 'sistemas',
                ) );
            ?>
            <div class="title-wrap">
                <div class="row">
                    <div class="col-9">
                        <a class="featured-title" href=" <?php echo get_permalink( get_page_by_path( 'acceso-sistemas' ) ); ?>">Accesos Directos</a>
                    </div>
                    <div class="col-3 text-right">
                        <a class="featured-button" href="#carouselSystemBf" role="button" data-slide="prev">
                            <i class="fa fa-angle-double-left"></i>
                        </a> 
                        <a class="featured-button" href="#carouselSystemBf" role="button" data-slide="next">
                            <i class="fa fa-angle-double-right"></i>
                        </a> 
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-12">
                    <div id="carouselSystemBf" class="carousel slide" data-ride="carousel">
                        <?php $count = 0; ?>
                        <div class="carousel-inner" >
                        <?php if ( $systems_loop->have_posts() ) : ?>
                            <?php while ( $systems_loop->have_posts() ) : $systems_loop->the_post();?>
                            <?php $imagen = esc_url(get_the_post_thumbnail_url(get_the_ID(),'medium'));?>
                            <div class="carousel-item <?php echo ($count == 0) ? 'active' : ''; ?>">
                                <?php if($imagen): ?>
                                    <a href="<?php echo get_field('enlace_del_sistema');?>" target="#">
                                        <img class="img-fluid" src="<?php echo $imagen; ?>" alt="<?php echo $imagen; ?>" style="width: 100%; max-height: 200px;"/>
                                    </a>
                                <?php else: ?>
                                    <a href="<?php echo get_field('enlace_del_sistema');?>" target="#">
                                        <img class="img-fluid" src="<?php echo get_parent_theme_file_uri( '/assets/images/fortaleza-blanco.png' ); ?>" alt="<?php echo get_parent_theme_file_uri( '/assets/images/fortaleza-blanco.png' ); ?>" style="width: 100%; max-height: 200px;"/>
                                    </a>
                                <?php endif; ?>
                            </div>
                            <?php $count++; ?>
                            <?php endwhile;?>
                        <?php else: ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
                        
            <div class="title-wrap">
                <div class="row">
                    <div class="col-9">
                        <h3 class="featured-title">
                            Bienvenida 
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                    $welcome_loop = new WP_Query( array(
                    'posts_per_page' => 5,
                    'category_name' => 'Bienvenida',
                    ) );
                ?>
                <div class="col-12">
                    <div id="carouselSystemBf" class="carousel slide" data-ride="carousel">
                        <?php $count = 0; ?>
                        <div class="carousel-inner" >
                        <?php if ( $welcome_loop->have_posts() ) : ?>
                            <?php while ( $welcome_loop->have_posts() ) : $welcome_loop->the_post();?>
                            <?php $imagen = esc_url(get_the_post_thumbnail_url(get_the_ID(),'full'));?>
                            <div class="carousel-item <?php echo ($count == 0) ? 'active' : ''; ?>">
                                <?php if($imagen): ?>
                                    <img class="img-fluid" src="<?php echo $imagen; ?>" alt="<?php echo $imagen; ?>" style="width: 100%; max-height: 200px;"/>
                                <?php else: ?>
                                    <img class="img-fluid" src="<?php echo get_parent_theme_file_uri( '/assets/images/fortaleza-blanco.png' ); ?>" alt="<?php echo get_parent_theme_file_uri( '/assets/images/fortaleza-blanco.png' ); ?>" style="width: 100%; max-height: 150px;"/>
                                <?php endif; ?>
                            </div>
                            <?php $count++; ?>
                            <?php endwhile;?>
                        <?php else:?>
                            <div class="pt-4" style="width: 100%; height: 150px; background-color: var(--azul-fortaleza);">
                                <p class="welcome-title">
                                   El Grupo Fortaleza te da la Bienvenida: 
                                </p>
                                <p class="welcome-name">
                                    <?php  $current_user = wp_get_current_user();
                                    printf( __( '%1s %2s', 'fortaleza' ), esc_html( $current_user->user_firstname ) , esc_html( $current_user->user_lastname ) );
                                    ?>
                                </p>
                                <p class="welcome-text">
                                    y le desea éxitos en sus funciones
                                </p>
                            </div> 
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
        
    </div>
</section>