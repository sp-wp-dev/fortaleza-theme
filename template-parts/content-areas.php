<?php 
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $page =  $post;
    $page_loop = new WP_Query( array(
    'paged'=> $paged,
    'category_name' => $page->post_name,
    ) );
?>
<div class="col-md-9 px-0">
    <div class="row title-container mx-0">
        <div class="col">
            <h2 class="title-page">
                <?php single_post_title(); ?>
            </h2>
        </div>
        <div id="breadcrumb" class="col text-right pr-2">
                    <?php bf_breadcrumbs(); ?>
        </div>
    </div>
    <?php if ( $page_loop->have_posts() ) : while ( $page_loop->have_posts() ) : $page_loop->the_post(); ?>
        <?php 
        $categories = get_the_category( $post->ID );
        $key= array_search($page->post_name, array_column($categories, 'slug'));
         if($key !== false){
             ?>
            <div class="card mb-2">
                <div class="card-body">
                    <div class="bf-card-title">
                        <?php the_title( '<h5 class="card-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h5>' ); ?>
                    </div>                            
                    <?php 
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail('post-thumbnails', array(
                                'class' => 'img-fluid mb-3'
                            ));
                        }
                    ?>
                    <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>" class="btn btn-primary">Más info...</a>
                        <p class="card-text text-right mb-0"><strong>Fecha de publicación : </strong><?php the_time('F j, Y'); ?>  </p>
                </div>
            </div>
             <?php 
            
         }?>
    <?php endwhile; 
    else : ?>
        <div class="bf-card-title">
            <h5 class="card-title"> No existe contenido </h5>
        </div>    
    <?php
    endif;  wp_reset_postdata();  ?>
    <!-- Paginación -->
    <?php 
    if($key !== false){
        echo bootstrap_pagination($page_loop); 
    }
    ?> 
</div>
<div class="col-md-3">
    <?php get_sidebar('page'); ?>  
</div>
