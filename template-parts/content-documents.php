<?php

    $post = $wp_query->get_queried_object();
    $post_parent= get_post($post->post_parent);
    $id_ancestor= get_post_ancestors( $post->ID )[1];
    $post_ancestor=get_post($id_ancestor);
    $slug_category=$post->post_name.'-'.$post_parent->post_name.'-'.$post_ancestor->post_name;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    //var_dump($slug_category);
    $page_loop = new WP_Query( array(
        'paged'=> $paged,
        'category_name' => $slug_category,
    ) );
?>
<!-- Entrada -->

<div class="col-md-9 px-0">
    <div class="row title-container mx-0">
        <div class="col">
            <h2 class="title-page">
                <?php single_post_title(); ?>
            </h2>
        </div>
        <div id="breadcrumb" class="col text-right pr-2">
            <?php bf_breadcrumbs(); ?>
        </div>
    </div>
    <?php if ( $page_loop->have_posts() ) : while ( $page_loop->have_posts() ) : $page_loop->the_post(); ?>
        <div class="card mb-2">
            <div class="card mb-2">
                <div class="card-body">
                    <div class="bf-card-title">
                        <?php the_title( '<h5 class="card-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h5>' ); ?>
                    </div>                            
                    <?php 
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail('post-thumbnails', array(
                                'class' => 'img-fluid mb-3'
                            ));
                        }
                    ?>
                    <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>" class="btn btn-primary">Más info...</a>
                        <p class="card-text text-right mb-0"><strong>Fecha de publicación : </strong><?php the_time('F j, Y'); ?>  </p>
                </div>
            </div>
        </div>
    <?php endwhile;
    else : ?>
        <div class="bf-card-title">
            <h5 class="card-title"> No existe contenido </h5>
        </div>    
    <?php
    endif; wp_reset_postdata();  ?>
    <!-- Paginación -->
    <?php echo bootstrap_pagination($page_loop); ?> 
</div>
<div class="col-md-3">
    <?php get_sidebar('page'); ?>  
</div> 