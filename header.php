<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <style>
            @media all and (min-width: 992px) {
            .navbar .nav-item .dropdown-menu{ display: none; }
            .navbar .nav-item:hover .nav-link{ color: #fff;  }
            .navbar .nav-item:hover .dropdown-menu{ display: block; }
            .navbar .nav-item .dropdown-menu{ margin-top:0; }
            }
        </style>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <!-- Header -->
        <header class="bf-header">
            <div class="bf-head-detail">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-md-2 col-xs-12">
                            <ul class="info-left mb-0">
                                <?php 
                                echo date_i18n( get_option( 'date_format' ) );
                                ?>
                                <span  id="time" class="time"></span>
                            </ul>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo do_shortcode('[bf_show_member]'); 
                            ?>
			            </div>
                        <div class="col-md-4 text-right">
                            <?php 
                            if(is_user_logged_in()){
                                $current_user = wp_get_current_user();
                                $user_roles = $current_user->roles;
                                if ( !in_array( 'subscriber', $user_roles, true ) ) {
                                    ?>
                                    <a name="" id="" class="btn btn-fortaleza mr-2" href="<?php echo get_home_url().'/wp-admin' ?>" role="button"> Administrar</a>
                                    <?php 
                                }
                            }
                            ?>
                         	<?php echo do_shortcode('[bf_formulario_login]'); ?>
			            </div>
                    </div>
                </div>
            </div>
            <?php get_template_part( 'template-parts/header/header', 'image' ); ?> 
            <!-- Navigation -->
            <nav id="navbar_top" class="navbar navbar-expand-lg d-block">
                <div class="navigation-bar">
                    <div class="navigation-bar-top">
                        <div class="container">
                            <button class="navbar-toggler menu-toggle" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="navbarCollapse"></button>
                            <span class="search-toggle"></span>
                        </div><!-- .container -->
                        <div class="search-bar">
                            <div class="container-fluid">
                                <div class="search-block off">
                                    <?php get_search_form(); ?>
                                </div><!-- .search-box -->
                            </div><!-- .container -->
                        </div><!-- .search-bar -->
                    </div><!-- .navigation-bar-top -->
                    <div class="navbar-main">
                        <div class="container-fluid">
                            <div class="collapse navbar-collapse" id="navbarCollapse">
                                <div class="bf-home-button">
                                    <a class="bf-home" href="<?php echo get_home_url() ?>" name=""><i class="fa fa-home"></i></a>
                                </div>
                                <div id="site-navigation" class="main-navigation nav-uppercase" role="navigation">
                                    <?php
                                    if ( has_nav_menu('header-menu') ) {
                                        wp_nav_menu( array(
                                            'theme_location'	=> 'header-menu',
                                            'container'			=> '',
                                            'items_wrap'		=> '<ul class="nav-menu navbar-nav d-lg-block">%3$s</ul>',
                                        ) );
                                    } else {
                                        wp_page_menu( array(
                                            'before' 			=> '<ul class="nav-menu navbar-nav d-lg-block">',
                                            'after'				=> '</ul>',
                                        ) );
                                    }
                                    ?>                                    
                                </div><!-- #site-navigation .main-navigation -->
                            </div><!-- .navbar-collapse -->
                            <div class="nav-search">
                                <span class="search-toggle"></span>
                            </div><!-- .nav-search -->
                        </div><!-- .container -->
                    </div><!-- .navbar-main -->
                </div><!-- .navigation-bar -->
            </nav><!-- .navbar -->

        </header>
        <section class="bf-exchange-rate">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 col-sm-4 bf-bn-title">
                        <h2>
                            Tipo de Cambio
                        </h2>
                    </div>
                    <div class="col-md-10 col-md-8 bf-news-slider">
                        
                        <marquee behavior="alternate" direction="left">
                            <?php
                                    $api_url = get_option( 'api_exchange_field' ).'ms-parameters/v1/exchange';
                                    $args = array('method' => 'GET',);
                                    $response = wp_remote_request( $api_url, $args );
                                    $api_response = json_decode( wp_remote_retrieve_body( $response ), true )['response'];
                                ?>
                                <strong>Tipo de Cambio Oficial : </strong><span class="px-2"><?php echo $api_response['officialExchange'] ?> Bs.</span> 
                                <strong>Compra : </strong> <span class="pr-2"><?php echo $api_response['saleExchange'] ?> Bs.</span> 
                                <strong>Venta : </strong> <span class="pr-2"><?php echo $api_response['buyExchange'] ?> Bs.</span> 
                                <strong>UFV : </strong> <span class="pr-4"><?php echo $api_response['ufvExchange'] ?> Bs.</span> 
                        </marquee>
                    </div>
                </div>
            </div>
        </section>
