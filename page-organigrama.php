<?php get_header(); ?>

    <div class="container-fluid mb-4">
        <div class="row m-2">



            <!-- Entrada -->
            <div class="col px-0">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    
                    <!-- Contenido -->
                    <div class="card">
                        <div class="card-body">
                            <div class="bf-card-title">
                                <h5 class="card-title"><?php the_title(); ?> </h5>
                            </div>                            
                            <?php 
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('post-thumbnails', array(
                                        'class' => 'img-fluid mb-3'
                                    ));
                                }
                            ?>
                            <?php the_content(); ?>
                            <p class="card-text text-right mb-0"><strong>Fecha de publicación : </strong><?php the_time('F j, Y'); ?>  </p>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
<?php get_footer();?>