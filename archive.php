<?php get_header(); ?>
    <div class="bf-categories">
        <div class="container-fluid mb-4">
            <div class="row m-2">
                <div class="col-md-9 px-0">
                    <div class="row title-container mx-0">
                        <div id="breadcrumb" class="col text-left px-2">
                                <a class="btn-breadcrumb" href="<?php echo get_home_url() ?>" name="volver atrás"><i class="fa fa-home"></i> Inicio </a>
                        </div>
                        <div id="breadcrumb" class="col text-right px-2">
                                <button class="btn-breadcrumb" onclick="history.back()" name="volver atrás"><i class="fa fa-undo"></i> atrás</button>
                        </div>
                    </div>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <!-- Contenido -->
                        <div class="card">
                            <div class="card-body">
                                <div class="bf-card-title">
                                    <?php the_title( '<h5 class="card-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h5>' ); ?>
                                </div>                            
                                <?php 
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('post-thumbnails', array(
                                            'class' => 'img-fluid mb-3'
                                        ));
                                    }
                                ?>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="btn btn-primary">Más info...</a>
                                <p class="card-text text-right mb-0"><strong>Fecha de publicación : </strong><?php the_time('F j, Y'); ?>  </p>
                            </div>
                        </div>
                    <?php endwhile; endif; ?>
                    <?php get_template_part('template-parts/content','pagination'); ?>
                </div>
                <div class="col-md-3">
                    <!-- Sidebar category -->
                    <?php get_sidebar('category'); ?>  
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>