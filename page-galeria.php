<?php get_header(); ?>

    <div class="container-fluid mb-4">
        <h4 class="text-center">Galeria </h4>
        <div class="row my-2 justify-content-md-center">
            <!-- Entrada -->
            <div class="col-lg-10 col-md-12">
                <?php
                    $news_loop = new WP_Query( array(
                    'category_name' => 'Galeria',
                    ) );
                    ?>
                <?php if ( $news_loop->have_posts() ) : while ( $news_loop->have_posts()) :  $news_loop->the_post(); ?>
                <?php 
                 ?>
                    <!-- Contenido -->
                    <div class="card my-2">
                        <div class="card-body">
                            <a class="bf-card-title" href="<?php the_permalink(); ?>">
                                <h5 class="card-title"><?php the_title(); ?> </h5>
                            </a>         
                            <div class="row">
                                <div class="col-md-12">
                                    <?php the_content(); ?>
                                </div>
                            </div>                  
                            <p class="card-text text-right mb-0"><strong>Fecha de publicación : </strong><?php the_time('F j, Y'); ?>  </p>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            </div>

            <!-- Sidebar Derecha -->
        </div>
        <h4 class="text-center">Videos </h4>
        <div class="row my-2 justify-content-md-center">
            <!-- Entrada -->
            <div class="col-lg-10 col-md-12">
                <div class="card my-2">
                    <div class="card-body">
                        <div class="row page-video">
                            <?php
                            $news_loop = new WP_Query( array(
                            'category_name' => 'video',
                            ) );
                            ?>
                            <?php if ( $news_loop->have_posts() ) : while ( $news_loop->have_posts()) :  $news_loop->the_post(); ?>
                            <div class="col-md-4 ">
                                <div class="bf-card-title">
                                    <h5 class="card-title"><?php the_title(); ?> </h5>
                                </div>         
                                <?php the_content(); ?>
                            </div>
                            <?php endwhile; endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>