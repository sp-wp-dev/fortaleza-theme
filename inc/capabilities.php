<?php
/**
 * Banco Fortaleza: Capabilities 
 *
 * @package WordPress
 * @subpackage fortaleza
 * @since Banco Fortaleza 1.0
 */

$role_object = get_role( 'editor');
$role_object->remove_cap( 'edit_pages' );
$role_object->remove_cap( 'manage_options' );