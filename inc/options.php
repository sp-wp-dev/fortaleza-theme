<?php
/**
 * Variable Entorno class
 *
 * @author Paul Sanchez <pssanchez@grupofortaleza.com.bo>
 */
if (isset($_POST['test'])) {
    bf_function_to_run();
}


function my_admin_menu() {
    add_submenu_page(
        'options-general.php',
        __( 'Variable de Entorno', 'my-textdomain' ),
        __( 'Variable de Entorno', 'my-textdomain' ),
        'manage_options',
        'enviroment-page',
        'admin_page_contents',
        3
    );
}

add_action( 'admin_menu', 'my_admin_menu' );


function admin_page_contents() {
    ?>
    <h1> <?php esc_html_e( 'Configuración de Variables de entorno del Grupo Fortaleza.', 'title-textdomain' ); ?> </h1>
    <form method="POST" action="options.php">
    <?php
    settings_fields( 'enviroment-page' );
    do_settings_sections( 'enviroment-page' );
    submit_button();
    ?>
    </form>
    <?php
}


add_action( 'admin_init', 'settings_init' );

function settings_init() {

    add_settings_section(
        'page_setting_section',
        __( 'Ajustes de variables', 'textdomain' ),
        'setting_section_callback_function',
        'enviroment-page'
    );

		add_settings_field(
		   'api_exchange_field', 
		   __( 'URL API Tipo de Cambio :', 'textdomain' ),
		   'setting_markup',
		   'enviroment-page',
		   'page_setting_section'
		);
		register_setting( 'enviroment-page', 'api_exchange_field' );

        add_settings_field(
            'api_salar_field', 
            __( 'URL API Salar :', 'textdomain' ),
            'setting_salar',
            'enviroment-page',
            'page_setting_section'
         );         
         register_setting( 'enviroment-page', 'api_salar_field' );

         add_settings_field(
            'user_salar_field', 
            __( 'Usuario Salar :', 'textdomain' ),
            'setting_salar_user',
            'enviroment-page',
            'page_setting_section'
         );
         register_setting( 'enviroment-page', 'user_salar_field' );

         add_settings_field(
            'pass_salar_field', 
            __( 'Contraseña Salar :', 'textdomain' ),
            'setting_salar_pass',
            'enviroment-page',
            'page_setting_section'
         );
         register_setting( 'enviroment-page', 'pass_salar_field' );

         add_settings_field(
            'button_salar_field', 
            __( 'Sincronizar datos :', 'textdomain' ),
            'update_data_salar',
            'enviroment-page',
            'page_setting_section'
         );
         register_setting( 'enviroment-page', 'button_salar_field' );
}


function setting_section_callback_function() {
}


function setting_markup() {
    ?>
    <input type="text" id="api_exchange_field" name="api_exchange_field" placeholder="https://10.0.2.123:4452/" value="<?php echo get_option( 'api_exchange_field' ); ?>" style="width: 50%;">
    <?php
}
function setting_salar() {
    ?>
    <input type="text" id="api_salar_field" name="api_salar_field" placeholder="https://salar.grupofortaleza.com.bo/" value="<?php echo get_option( 'api_salar_field' ); ?>" style="width:50%;">
    <?php
}
function setting_salar_user() {
    ?>
    <input type="text" id="user_salar_field" name="user_salar_field" placeholder="usuario" value="<?php echo get_option( 'user_salar_field' ); ?>" style="width:50%;">
    <?php
}
function setting_salar_pass() {
    ?>
    <input type="password" id="pass_salar_field" name="pass_salar_field" placeholder="" value="<?php echo get_option( 'pass_salar_field' ); ?>" style="width:50%;">
    <?php
}
function update_data_salar() {
    ?>
    <button type="button" name="" id="btn-update" class="button button-primary" btn-lg btn-block">Actualizar Datos</button>
    <br>
    <span>Guarde los datos antes de actualizar</span>
    <?php
}