<?php
    class Bf_birthday
    {
    
        function clearData (){
            global $wpdb;
            $table_name = $wpdb->prefix . "birthdays";
            $sql = "TRUNCATE TABLE `$table_name`;" ;
            $wpdb->query( $sql );

        }
        function setData (){
            global $wpdb;
            $table_birthday = $wpdb->prefix."birthdays";
            $table_user_salar = $wpdb->prefix."user_salar";
            $users = $wpdb->get_results( "SELECT * FROM $table_user_salar" );
            foreach ($users as  $user) {

                $wpdb->insert($table_birthday,array(
                    'name' => $user->name,
                    'date' => $user->birthdate,
                    'email' => $user->email
                ));
            }
        }
    }
?>