<?php
    class Salar
    {
        public $api_salar;
        public $token;
    
        function __construct() {
            $this->api_salar = get_option( 'api_salar_field' );
        }

        function authentication  (){
            $body = [
                'IdEmpresa'  => 123,
                'Cuenta'  => get_option( 'user_salar_field' ),
                'Contrasena' => get_option( 'pass_salar_field' ),
            ];
            $body = wp_json_encode( $body );
            $options = [
                'body'        => $body,
                'headers'     => [
                    'Content-Type' => 'application/json',
                    ]
                ];
            $response = wp_remote_post( $this->api_salar.'api/Autenticacion/Autenticar', $options );
            $response_code = wp_remote_retrieve_response_code( $response );
            $this->token = json_decode( wp_remote_retrieve_body( $response ), true )['Token'];
            if($this->token ==null){
                var_dump("false");
            }else{
                var_dump("true");
            }
        }

        function getData  (){
            if(empty($this->token)||$this->token==null){
                
                $this->authentication();
            }
            $body = [
                'FechaIngresoInicio'  => null,
                'FechaIngresoFin'  => null,
                'Estado' => 1,
            ];
            $body = wp_json_encode( $body );
            $options = [
                'body'        => $body,
                'headers'     => [
                    'Content-Type' => 'application/json',
                    'Authorization'=> 'Bearer '.$this->token
                ]
            ];
            $response = wp_remote_post( $this->api_salar.'api/Colaboradores/Consultar', $options );
            $result = json_decode( wp_remote_retrieve_body( $response ), true )['Resultado'];
            return $result;
        }
        function getDataPossition  (){
            if(empty($this->token)||$this->token==null){
                $this->authentication();
            }
            $body = [];
            $body = wp_json_encode( $body );
            $options = [
                'body'        => $body,
                'headers'     => [
                    'Content-Type' => 'application/json',
                    'Authorization'=> 'Bearer '.$this->token
                ]
            ];
            $response = wp_remote_post( $this->api_salar.'api/Cargos/ConsultarCargos', $options );
            if(is_wp_error($response)){
                return $$response;
            }
            $result = json_decode( wp_remote_retrieve_body( $response ), true )['Resultado'];
            return $result;
        }

        function clearTable ($table){
            global $wpdb;
            $table_name = $wpdb->prefix . $table;
            $sql = "TRUNCATE TABLE `$table_name`;" ;
            $wpdb->query( $sql );
        }
        function saveData  (){
            global $wpdb;
            $result = $this->getData();
            if(!is_wp_error($result)){
                $table_name = $wpdb->prefix . "user_salar";
                $rows = $wpdb->get_results( "SELECT COUNT(*) FROM $table_name" );
                if(!empty($rows)){
                    $this->clearTable("user_salar");
                }
                $table = $wpdb->prefix."user_salar";
                foreach ($result as $key => $user) {
                    $wpdb->insert($table,array(
                        'name' => $user["PrimerNombre"]." ".$user["SegundoNombre"],
                        'lastname' => $user["ApellidoPaterno"]." ".$user["ApellidoMaterno"],
                        'document_id' => $user["NumeroDocumento"],
                        'extension' => $user["ExtensionDocumento"],
                        'birthdate' => $user["FechaNacimiento"],
                        'email' => $user["CorreoLaboral"],
                        'position' => $user["Cargo"],
                        'office' => $user["Oficina"],
                        'work_phone' => $user["TelefonoFijoLaboral"],
                        'internal_phone' => $user['DatosPersonalizados'][0]['Valor'],
                        'personal_phone' => $user["TelefonoMovilPersonal"],
                    ));
                }
            }
        }
        function savePosition  (){
            global $wpdb;
            $result = $this->getDataPossition();
            if(!is_wp_error($result)){
                $table_name = $wpdb->prefix . "position_salar";
                $rows = $wpdb->get_results( "SELECT COUNT(*) FROM $table_name" );
                if(!empty($rows)){
                    $this->clearTable("position_salar");
                }
                $table = $wpdb->prefix."position_salar";
                foreach ($result as $key => $user) {
                    $wpdb->insert($table,array(
                        'erp_code' => $user["CodigoERP"],
                        'name' => $user["Nombre"],
                        'name_templates' => $user["NombrePlanillas"],
                        'description' => $user["Descripcion"],
                        'type_position' => $user["CodigoERPTipoCargo"],
                        'unit_organizational' => $user["CodigoERPUnidadOrganizativa"],
                        'position_superior' => $user["CodigoERPCargoSuperior"],
                        'position_approber' => $user["CodigoERPCargoAprobador"],
                        'state' => $user["Estado"],
                    ));
                }
            }
        }
    }

?>
