<?php
    class Salar_Installer{
        
        static function install() {
            global $wpdb;
            $table_name = $wpdb->prefix . "user_salar"; 
            //dbDelta is responsible to alter the table if necessary
            $sql = "CREATE TABLE `$table_name` (
                      id bigint(20) NOT NULL AUTO_INCREMENT,
                      name varchar(20) ,
                      lastname varchar(20),
                      document_id varchar(20) DEFAULT NULL,
                      extension varchar(10) DEFAULT NULL,
                      birthdate date,
                      email varchar(30) DEFAULT NULL,
                      position varchar(30) DEFAULT NULL,
                      office varchar(30) DEFAULT NULL,
                      work_phone varchar(20) DEFAULT NULL,
                      internal_phone varchar(20) DEFAULT NULL,
                      personal_phone varchar(20) DEFAULT NULL,
                      PRIMARY KEY id (id)
                    ) DEFAULT CHARSET=utf8mb4;";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
            $table_cargos= $wpdb->prefix . "position_salar"; 
            $sql = "CREATE TABLE `$table_cargos` (
                      id bigint(20) NOT NULL AUTO_INCREMENT,
                      erp_code varchar(20),
                      name varchar(50),
                      name_templates varchar(50),
                      description varchar(20) DEFAULT NULL,
                      type_position varchar(10) DEFAULT NULL,
                      unit_organizational varchar(10) DEFAULT NULL,
                      position_superior varchar(10) DEFAULT NULL,
                      position_approber varchar(10) DEFAULT NULL,
                      state varchar(20) DEFAULT NULL,
                      PRIMARY KEY id (id)
                    ) DEFAULT CHARSET=utf8mb4;";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );

            return;
        }

        static function uninstall() {
            //delete all of our user meta
            // $users = get_users( array( 'fields' => 'id' ) );
            // foreach ( $users as $id ) {
            //     delete_user_meta( $id, 'birthday_id' );
            // }

            //drop a custom db table
            global $wpdb;
            $table_name = $wpdb->prefix . "user_salar";
            $sql = "DROP TABLE IF EXISTS `$table_name`;" ;
            $wpdb->query( $sql );
        }

        static function activate() {

            return Salar_Installer::install();
        }

        static function deactivate() {
           
        }
    }
