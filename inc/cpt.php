<?php
/**
 * Banco Fortaleza: CPT
 *
 * @package WordPress
 * @subpackage fortaleza
 * @since Banco Fortaleza 1.0
 */

if ( ! function_exists('bf_sistemas') ) {

  // Register Custom Post Type
  function bf_sistemas() {
  
    $labels = array(
      'name'                  => _x( 'Sistemas', 'Post Type General Name', 'text_domain' ),
      'singular_name'         => _x( 'Sistema', 'Post Type Singular Name', 'text_domain' ),
      'menu_name'             => __( 'Sistemas', 'text_domain' ),
      'name_admin_bar'        => __( 'Sistemas', 'text_domain' ),
      'archives'              => __( 'Archivos de Sistemas', 'text_domain' ),
      'attributes'            => __( 'Atributos de sistema', 'text_domain' ),
      'parent_item_colon'     => __( 'Sistema padre', 'text_domain' ),
      'all_items'             => __( 'Todos los Sistemas', 'text_domain' ),
      'add_new_item'          => __( 'Añadir nuevo Sistema', 'text_domain' ),
      'add_new'               => __( 'Añadir Nuevo', 'text_domain' ),
      'new_item'              => __( 'Nuevo Sistema', 'text_domain' ),
      'edit_item'             => __( 'Editar Sistema', 'text_domain' ),
      'update_item'           => __( 'Actualizar Sistema', 'text_domain' ),
      'view_item'             => __( 'Ver Sistema', 'text_domain' ),
      'view_items'            => __( 'Ver Sistema', 'text_domain' ),
      'search_items'          => __( 'Buscar Sistema', 'text_domain' ),
      'not_found'             => __( 'No Encontrado', 'text_domain' ),
      'not_found_in_trash'    => __( 'No encontrado en la papelera', 'text_domain' ),
      'featured_image'        => __( 'Imagen Descataca', 'text_domain' ),
      'set_featured_image'    => __( 'Configurar imagen destacada', 'text_domain' ),
      'remove_featured_image' => __( 'Borrar imagen Destacada', 'text_domain' ),
      'use_featured_image'    => __( 'Usar como imagen destacada', 'text_domain' ),
      'insert_into_item'      => __( 'Insertar en el Sistema', 'text_domain' ),
      'uploaded_to_this_item' => __( 'Actualizar en este Sistema', 'text_domain' ),
      'items_list'            => __( 'Listado de Sistemas', 'text_domain' ),
      'items_list_navigation' => __( 'Lista navegable de libros', 'text_domain' ),
      'filter_items_list'     => __( 'filtro de lista de libros', 'text_domain' ),
    );
    $args = array(
      'label'                 => __( 'Sistema', 'text_domain' ),
      'description'           => __( 'Entradas de Sistemas', 'text_domain' ),
      'labels'                => $labels,
      'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
      'taxonomies'            => array( 'category', 'post_tag' ),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'menu_icon'             => 'dashicons-welcome-widgets-menus',
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => true,
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'post',
    );
    register_post_type( 'Sistemas', $args );
  
  }
  add_action( 'init', 'bf_sistemas', 0 );
  
  }

  if ( ! function_exists('bf_comunicados') ) {

    // Register Custom Post Type
    function bf_comunicados() {
    
        $labels = array(
            'name'                  => _x( 'Comunicados', 'Post Type General Name', 'text_domain' ),
            'singular_name'         => _x( 'Comunicado', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'             => __( 'Comunicados', 'text_domain' ),
            'name_admin_bar'        => __( 'Comunicados', 'text_domain' ),
            'archives'              => __( 'Archivos', 'text_domain' ),
            'attributes'            => __( 'Atributos', 'text_domain' ),
            'parent_item_colon'     => __( 'Padre', 'text_domain' ),
            'all_items'             => __( 'Todas', 'text_domain' ),
            'add_new_item'          => __( 'Add New Item', 'text_domain' ),
            'add_new'               => __( 'Añadir', 'text_domain' ),
            'new_item'              => __( 'Nuevo', 'text_domain' ),
            'edit_item'             => __( 'Editar', 'text_domain' ),
            'update_item'           => __( 'Actualizar', 'text_domain' ),
            'view_item'             => __( 'Ver', 'text_domain' ),
            'view_items'            => __( 'Ver Comunicados', 'text_domain' ),
            'search_items'          => __( 'Buscar', 'text_domain' ),
            'not_found'             => __( 'No encontrado', 'text_domain' ),
            'not_found_in_trash'    => __( 'No encontrado en la papelra', 'text_domain' ),
            'featured_image'        => __( 'Imagen destacada', 'text_domain' ),
            'set_featured_image'    => __( 'Configurar Imagen Destacada', 'text_domain' ),
            'remove_featured_image' => __( 'Borrar imagen destacada', 'text_domain' ),
            'use_featured_image'    => __( 'Usar como imagen destacada', 'text_domain' ),
            'insert_into_item'      => __( 'Insertar en', 'text_domain' ),
            'uploaded_to_this_item' => __( 'Actualizar en este', 'text_domain' ),
            'items_list'            => __( 'Listado de', 'text_domain' ),
            'items_list_navigation' => __( 'Lista navegable', 'text_domain' ),
            'filter_items_list'     => __( 'Filtro de Lista', 'text_domain' ),
        );
        $args = array(
            'label'                 => __( 'Comunicado', 'text_domain' ),
            'description'           => __( 'Entradas de Comunicados', 'text_domain' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
            'taxonomies'            => array( 'category', 'post_tag' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-rss',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'post',
        );
        register_post_type( 'comunicados', $args );
    
    }
    add_action( 'init', 'bf_comunicados', 0 );
    
    }