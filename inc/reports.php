<?php
/**
 * Reportes por usuario
 *
 * @author Paul Sanchez <pssanchez@grupofortaleza.com.bo>
 */

function menu_report() {
    add_submenu_page(
        'options-general.php',
        __( 'Reportes', 'my-textdomain' ),
        __( 'Reportes', 'my-textdomain' ),
        'manage_options',
        'users-reports',
        'admin_reports',
        3
    );
}
add_action( 'admin_menu', 'menu_report' );


function admin_reports() {
    ?>
    <h1> <?php esc_html_e( 'Reportes de usuarios', 'title-textdomain' ); ?> </h1>
    <form method="POST" action="options.php">
    <?php
    settings_fields( 'users-reports' );
    do_settings_sections( 'users-reports' );
    submit_button();
    ?>
    </form>
    <?php
}


add_action( 'admin_init', 'settings_init_report' );

function settings_init_report() {

    add_settings_section(
        'report_section',
        __( 'Reportes de usuarios', 'textdomain' ),
        'report_section_callback_function',
        'users-reports'
    );

		add_settings_field(
		   'users_field', 
		   __( 'Usuarios :', 'textdomain' ),
		   'report_users',
		   'users-reports',
		   'report_section'
		);
		register_setting( 'users-reports', 'users_field' );

        // add_settings_field(
        //     'api_salar_field', 
        //     __( 'URL API Salar :', 'textdomain' ),
        //     'setting_salar',
        //     'enviroment-page',
        //     'page_setting_section'
        //  );         
        //  register_setting( 'enviroment-page', 'api_salar_field' );

        //  add_settings_field(
        //     'user_salar_field', 
        //     __( 'Usuario Salar :', 'textdomain' ),
        //     'setting_salar_user',
        //     'enviroment-page',
        //     'page_setting_section'
        //  );
        //  register_setting( 'enviroment-page', 'user_salar_field' );

        //  add_settings_field(
        //     'pass_salar_field', 
        //     __( 'Contraseña Salar :', 'textdomain' ),
        //     'setting_salar_pass',
        //     'enviroment-page',
        //     'page_setting_section'
        //  );
        //  register_setting( 'enviroment-page', 'pass_salar_field' );
}


function report_section_callback_function() {
}


function report_users() {
    ?>
    <input type="text" id="api_exchange_field" name="api_exchange_field" placeholder="https://10.0.2.123:4452/" value="<?php echo get_option( 'api_exchange_field' ); ?>" style="width: 50%;">
    <?php
}