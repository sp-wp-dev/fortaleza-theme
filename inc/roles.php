<?php
/**
 * Banco Fortaleza: Roles
 *
 * @package WordPress
 * @subpackage fortaleza
 * @since Banco Fortaleza 1.0
 */


function my_auth_login ($user_login, WP_User $user) {
    function grupos( $item) {
      if('OU' == substr($item,0,2) ) {
        return true;
      }
      else {
        return false;
      }
    }
    
    $key = 'mo_ldap_user_dn';
    $single = true;
    $user_ldap = get_user_meta( $user->ID , $key, $single ); 
    $array_ldap = explode(",", $user_ldap);
    $array_ldap_ou = array_filter($array_ldap, "grupos");
    $display_name = substr($array_ldap[0], 3);
    //Actualizamos al Usuario  Nombre y Apellido
    $user_name = explode(" ", $display_name);
    if(count($user_name) >3){
        $first_name = $user_name[0]. ' '. $user_name[1];
        $last_name = $user_name[2]. ' '. $user_name[3];
    }
    else{
        $first_name = $user_name[0];
        $last_name = $user_name[1]. ' '. $user_name[2];
    }
    $args = array(
        'ID'           => $user->ID ,
        'display_name' => $display_name,
        'first_name' => $first_name,
        'last_name' => $last_name,

      );
    $user_data =wp_update_user( $args );

    //OU[2] -> contiene el grupo
    switch ($array_ldap_ou[2]) {
        case 'OU=Tecnologia':
            $group_user_ldap= 'Tecnologia';
        break;
        case 'OU=Auditoria':
            $group_user_ldap= 'Auditoria';
        break;
        case 'OU=Contabilidad Nacional':
            $group_user_ldap= 'Contabilidad';
        break;
        case 'OU=Finanzas':
            $group_user_ldap= 'Finanzas';
        break;
        case 'OU=Gerencia General':
            $group_user_ldap= 'Grupo General';
        break;
        case 'OU=Legal':
            $group_user_ldap= 'Legal';
        break;
        case 'OU=Negocios':
            $group_user_ldap= 'Negocios';
        break;
        case 'OU=Operaciones':
            $group_user_ldap= 'Operaciones';
        break;
        case 'OU=Riesgos':
            $group_user_ldap= 'Riesgos';
        break;
        case 'OU=RRHH':
            $group_user_ldap= 'Talento Humano';
        break;
        default:
            $group_user_ldap= 'Grupo Otros';
            break;
    }
    $group = $GLOBALS['wpdb']->get_results( "SELECT * FROM wp_groups_group WHERE name='$group_user_ldap'", OBJECT );
    if(count($group)>0) {
        $group_id =  $group[0]->group_id;
    }else {
        // Si no tiene un Grupo 
        $group_id = 0;
    }
    $results = $GLOBALS['wpdb']->update('wp_groups_user_group',
    array('group_id' => $group_id),
    array( 'user_id' => $user->ID )
    );

    // exit;  
  }
  add_action('wp_login', 'my_auth_login',10,2);