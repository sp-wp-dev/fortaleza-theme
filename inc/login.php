<?php

function bf_formulario_login_shortcode( $atts, $content = null ) { 
  extract( shortcode_atts( array('redirect' => ''), $atts ) );
  if (!is_user_logged_in()) {
      return '<span id="login" class="login">
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">
          <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
          <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
      </svg>
      <a href="'.wp_login_url().'">Iniciar Sesión</a>
      </span>'; 
  } 
  else { 
    echo '<span id="logout" class="logout">
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-power" viewBox="0 0 16 16">
          <path d="M7.5 1v7h1V1h-1z"/>
          <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z"/>
      </svg>
      <a href="'.wp_logout_url().'">Cerrar Sesión</a>
    </span>'; 
  } 
} 
   


function my_login_redirect( $url, $request, $user ){
  if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
      if( $user->has_cap( 'administrator' ) ) {
          $url = admin_url();
      } else {
          $url = home_url('');
      }
  }
  return $url;
}

add_filter('login_redirect', 'my_login_redirect', 10, 3 );

function bf_show_member_info () {
  $current_user = wp_get_current_user();
  if ($current_user->ID != 0) {
    ob_start();
    ?>
    <span class="mr-3">
      <?php printf( __( '<strong>Usuario:</strong> %1s', 'fortaleza' ), esc_html( $current_user->display_name ) );  ?>
    </span>
    <span class="">
      <?php printf( __( '<strong>email:</strong> %s', 'fortaleza' ), esc_html( $current_user->user_email ) ); ?>
    </span>
    <?php
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
  }
  else{
    return;
  }
}

function bf_add_shortcodes() { 
  add_shortcode( 'bf_formulario_login', 'bf_formulario_login_shortcode' ); 
  add_shortcode( 'bf_show_member', 'bf_show_member_info');
} 

add_action( 'init', 'bf_add_shortcodes' );