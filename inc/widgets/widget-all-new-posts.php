<?php
if (!class_exists('Bf_All_New_Posts')) :
    /**
     * Adds Bf_All_New_Posts widget.
     */
    class Bf_All_New_Posts extends Bf_Widget_Base
    {
        /**
         * Sets up a new widget instance.
         *
         * @since 0.1
         */
        function __construct()
        {
            $this->text_fields = array('bf-categorised-posts-title', 'bf-posts-number', 'bf-excerpt-length');
            $this->select_fields = array('bf-select-category');

            $widget_ops = array(
                'classname' => 'bf-all-post',
                'description' => __('Muestra los ultimos posts  ', 'bf'),
                'customize_selective_refresh' => true,
            );

            parent::__construct('bf_all_new_posts', __('Fortaleza: Todos los post', 'bf'), $widget_ops);
        }

        /**
         * Front-end display of widget.
         *
         * @see WP_Widget::widget()
         *
         * @param array $args Widget arguments.
         * @param array $instance Saved values from database.
         */

        public function widget($args, $instance)
        {
            $instance = parent::bf_sanitize_data($instance, $instance);
            /** This filter is documented in wp-includes/default-widgets.php */

            $title = apply_filters('widget_title', $instance['bf-categorised-posts-title'], $instance, $this->id_base);

            $number_of_posts = 5;
            $category = isset($instance['bf-select-category']) ? $instance['bf-select-category'] : '0';

            // open the widget container
            echo $args['before_widget'];
            ?>
            <!-- mg-posts-sec mg-posts-modul-3 -->
            <div class="container-full">
                <?php if (!empty($title)):
                    echo $args['before_widget'];
                    if ( $title ) {
                        echo $args['before_title'] . $title . $args['after_title'];
                }
                endif; ?>           
                <?php
                $news_loop = bf_all_get_posts(5);
                ?>
                <div class="container">
                    <div class="row bf-wid-content pt-2">
                        <div class="col-12">
                            <?php if ( $news_loop->have_posts() ) : ?>
                                <?php while ( $news_loop->have_posts() ) : $news_loop->the_post();?>
                                <div class="row mb-2">
                                    <div class="col-md-4 text-center">
                                        <?php
                                            $imagen = esc_url(get_the_post_thumbnail_url(get_the_ID()));
                                            if($imagen): ?>
                                                <a href="<?php echo get_permalink($post->ID);?>">
                                                    <img class="img-fluid" src="<?php echo $imagen; ?>" alt="<?php echo $imagen; ?>" style="height: 75px;"/>
                                                </a>
                                                <?php
                                            else:?>
                                                <a href="<?php echo get_permalink($post->ID);?>">
                                                    <img class="img-fluid" src="<?php echo get_parent_theme_file_uri( '/assets/images/fortaleza.jpg' );?>" alt="<?php echo $imagen; ?>"/>
                                                </a>
                                                <?php
                                            endif;	
                                        ?>
                                    </div>
                                    <div class="col-md-8 pl-0 news-widget">
                                        <div class="row">
                                            <div class="col news-title-text">
                                                <h6 class="text-capitalize"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6> 
                                            </div>
                                            
                                        </div>
                                        <div class="row ">
                                            <div class="col text-right text-capitalize">
                                                <p class="news-content-text"><i class="fa fa-clock-o"></i> <?php the_time('F j, Y'); ?>  </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endwhile;?>
                            <?php else: ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // mg-posts-sec mg-posts-modul-3 --> 

                
                

            <?php
            //print_pre($all_posts);

            // close the widget container
            echo $args['after_widget'];
        }

        /**
         * Back-end widget form.
         *
         * @see WP_Widget::form()
         *
         * @param array $instance Previously saved values from database.
         */
        public function form($instance)
        {
            $this->form_instance = $instance;
            $categories = bf_get_terms();
            if (isset($categories) && !empty($categories)) {
                // generate the text input for the title of the widget. Note that the first parameter matches text_fields array entry
                echo parent::bf_generate_text_input('bf-categorised-posts-title', 'Titulo', 'Ultimas Noticias');
                // echo parent::bf_generate_select_options('bf-select-category', __('Seleccionar Categoria', 'bf'), $categories);

            }
        }
    }
endif;