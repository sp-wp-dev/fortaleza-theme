<?php

// Load widget base.
require_once get_template_directory() . '/inc/widgets/widgets-base.php';

/* Theme Widget sidebars. */
require get_template_directory() . '/inc/widgets/widgets-common-functions.php';

/* Theme Widgets*/
// require get_template_directory() . '/inc/ansar/widgets/widget-posts-carousel.php';
require get_template_directory() . '/inc/widgets/widget-all-new-posts.php';
require get_template_directory() . '/inc/widgets/widget-systems-posts.php';
require get_template_directory() . '/inc/widgets/widget-page-navigation.php';



/* Register site widgets */
if ( ! function_exists( 'bf_widgets' ) ) :
    /**
     * Load widgets.
     *
     * @since 1.0.0
     */
    function bf_widgets() {
        // register_widget( 'Newsup_Posts_Carousel' );
        register_widget( 'Bf_All_New_Posts' );
        register_widget( 'Bf_Systems_Posts' );
        register_widget( 'Bf_Page_Navigation' );
    }
endif;
add_action( 'widgets_init', 'bf_widgets' );
