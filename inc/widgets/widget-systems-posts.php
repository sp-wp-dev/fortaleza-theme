<?php
if (!class_exists('Bf_Systems_Posts')) :
    /**
     * Adds Bf_Systems_Posts widget.
     */
    class Bf_Systems_Posts extends Bf_Widget_Base
    {
        /**
         * Sets up a new widget instance.
         *
         * @since 0.1
         */
        function __construct()
        {
            $this->text_fields = array('bf-categorised-posts-title', 'bf-posts-number', 'bf-excerpt-length');
            $this->select_fields = array('bf-select-category');

            $widget_ops = array(
                'classname' => 'bf-systems-post',
                'description' => __('Muestra los sistemas   ', 'bf'),
                'customize_selective_refresh' => true,
            );

            parent::__construct('bf_systems_posts', __('Fortaleza: Sistemas', 'bf'), $widget_ops);
        }

        /**
         * Front-end display of widget.
         *
         * @see WP_Widget::widget()
         *
         * @param array $args Widget arguments.
         * @param array $instance Saved values from database.
         */

        public function widget($args, $instance)
        {
            $instance = parent::bf_sanitize_data($instance, $instance);
            /** This filter is documented in wp-includes/default-widgets.php */

            $title = apply_filters('widget_title', $instance['bf-categorised-posts-title'], $instance, $this->id_base);

            // open the widget container
            echo $args['before_widget'];
            ?>
            <!-- mg-posts-sec mg-posts-modul-3 -->
            <div class="container">
                <div>
                <?php if (!empty($title)):?>
                <div class="title-wrap">
                    <div class="row">
                        <div class="col-9 bf-wid-title">
                            <?php if (!empty($title)): ?>
                                <h6 class="">
                                    <?php echo esc_html($title);?>
                                </h6>
                            <?php endif; ?>
                        </div>
                        <div class="col-3 text-right">
                            <a class="featured-button" href="#carouselSystemW" role="button" data-slide="prev">
                                <i class="fa fa-angle-double-left"></i>
                            </a> 
                            <a class="featured-button" href="#carouselSystemW" role="button" data-slide="next">
                                <i class="fa fa-angle-double-right"></i>
                            </a> 
                        </div>
                    </div>
                </div>
                <?php endif; ?>       
                <?php
                $systems_loop = new WP_Query( array(
                'posts_per_page' => 5,
                'post_type' => 'sistemas',
                ) );
                ?>
                <div class="container">
                    <div class="row mb-2">
                        <div class="col-12">
                            <div id="carouselSystemW" class="carousel slide" data-ride="carousel">
                                <?php $count = 0; ?>
                                <div class="carousel-inner" >
                                <?php if ( $systems_loop->have_posts() ) : ?>
                                    <?php while ( $systems_loop->have_posts() ) : $systems_loop->the_post();?>
                                    <?php $imagen = esc_url(get_the_post_thumbnail_url(get_the_ID(),'medium'));?>
                                    <div class="carousel-item <?php echo ($count == 0) ? 'active' : ''; ?>">
                                        <?php if($imagen): ?>
                                            <a href="<?php echo get_field('enlace_del_sistema');?>" target="#">
                                                <img class="img-fluid" src="<?php echo $imagen; ?>" alt="<?php echo $imagen; ?>" style="width: 100%; max-height: 200px;"/>
                                            </a>
                                        <?php else: ?>
                                            <a href="<?php echo get_field('enlace_del_sistema');?>" target="#">
                                                <img class="img-fluid" src="<?php echo get_parent_theme_file_uri( '/assets/images/fortaleza-blanco.png' ); ?>" alt="<?php echo get_parent_theme_file_uri( '/assets/images/fortaleza-blanco.png' ); ?>" style="width: 100%; max-height: 200px;"/>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                    <?php $count++; ?>
                                    <?php endwhile;?>
                                <?php else: ?>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // mg-posts-sec mg-posts-modul-3 --> 

                
                

            <?php
            //print_pre($all_posts);

            // close the widget container
            echo $args['after_widget'];
        }

        /**
         * Back-end widget form.
         *
         * @see WP_Widget::form()
         *
         * @param array $instance Previously saved values from database.
         */
        public function form($instance)
        {
            $this->form_instance = $instance;
            $categories = bf_get_terms();
            if (isset($categories) && !empty($categories)) {
                // generate the text input for the title of the widget. Note that the first parameter matches text_fields array entry
                echo parent::bf_generate_text_input('bf-categorised-posts-title', 'Titulo', 'Sistemas');
                // echo parent::bf_generate_select_options('bf-select-category', __('Seleccionar Categoria', 'bf'), $categories);

            }
        }
    }
endif;