<?php
if (!class_exists('Bf_Page_Navigation')) :
    /**
     * Adds Bf_Page_Navigation widget.
     */
    class Bf_Page_Navigation extends Bf_Widget_Base
    {
        /**
         * Sets up a new widget instance.
         *
         * @since 0.1
         */
        function __construct()
        {
            $this->text_fields = array('bf-navegation-title');

            $widget_ops = array(
                'classname' => 'bf-page-navigation',
                'description' => __('Navegación  ', 'bf'),
                'customize_selective_refresh' => true,
            );

            parent::__construct('bf_page_navigation', __('Fortaleza: Navegación de paginas', 'bf'), $widget_ops);
        }

        /**
         * Front-end display of widget.
         *
         * @see WP_Widget::widget()
         *
         * @param array $args Widget arguments.
         * @param array $instance Saved values from database.
         */

        public function widget($args, $instance)
        {
            $instance = parent::bf_sanitize_data($instance, $instance);
            /** This filter is documented in wp-includes/default-widgets.php */

            $title = apply_filters('widget_title', $instance['bf-navegation-title'], $instance, $this->id_base);

            $number_of_posts = 5;
            $category = isset($instance['bf-select-category']) ? $instance['bf-select-category'] : '0';

            // open the widget container
            echo $args['before_widget'];
            ?>
            <div class="container-full">
                <?php if (!empty($title)):
                if ( $title ) {
                    echo $args['before_title'] . $title . $args['after_title'];
                }
                endif; ?>       
                <?php
                global $post;
                $pages = get_pages( array( 'parent' => $post->post_parent) );
                $exclude_page = Array('Prueba','inicio','noticias','organigrama','capacitaciones','galeria','directorio','busqueda-personas','convocatorias-internas','my-calendar','memorias','funcionarios-oficina-nacional','acceso-sistemas','fortaleza-leasing','controladora');
                ?>
                <div class="container">
                    <div class="row bf-wid-content pt-2">
                        <div class="col-12">
                            <ul>
                                <?php 
                                foreach ( $pages as $page ) { // 
                                    if(array_search($page->post_name, $exclude_page)==null){
                                    $option = '<li> <a href="' . get_page_link( $page->ID ) . '">';
                                    $option .= $page->post_title;
                                    $option .= '</a></li>';
                                    echo $option;
                                    }
                                }?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo $args['after_widget'];
        }

        /**
         * Back-end widget form.
         *
         * @see WP_Widget::form()
         *
         * @param array $instance Previously saved values from database.
         */
        public function form($instance)
        {
            $this->form_instance = $instance;
                // generate the text input for the title of the widget. Note that the first parameter matches text_fields array entry
                echo parent::bf_generate_text_input('bf-navegation-title', 'Titulo', 'Navegacion');
                // echo parent::bf_generate_select_options('bf-select-category', __('Seleccionar Categoria', 'bf'), $categories);

        }
    }
endif;