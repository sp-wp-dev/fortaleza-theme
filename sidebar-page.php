<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Fortaleza
 */

if ( ! is_active_sidebar( 'sidebar-page' ) ) {
	return;
}
?>

<aside id="secondary" class="aside-page bf-sidebar" role="complementary">
	<div id="sidebar-page" class="aside-page bf-sidebar">
		<?php dynamic_sidebar( 'sidebar-page' ); ?>
	</div>
</aside><!-- #secondary -->
