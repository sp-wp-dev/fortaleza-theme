<?php get_header(); ?>
    <div class="bf-page">
        <div class="container-fluid mb-4">
            <div class="row m-2">
                <?php 
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $page_loop = new WP_Query( array(
                    'paged'=> $paged,
                    'category_name' => $post->post_name,
                    ) );
                ?>
                <!-- Entrada -->
                <div class="col px-0">
                    <div class="row title-container mx-0">
                        <div class="col">
                            <h2 class="title-page">
                                <?php single_post_title(); ?>
                            </h2>
                        </div>
                        <div id="breadcrumb" class="col text-right pr-2">
                                    <?php bf_breadcrumbs(); ?>
                        </div>
                    </div>
                    <?php if ( $page_loop->have_posts() ) : while ( $page_loop->have_posts() ) : $page_loop->the_post(); ?>
                        <div class="card mb-2">
                            <div class="card-body">
                                <div class="bf-card-title">
                                    <h5 class="card-title"><?php the_title(); ?> </h5>
                                </div>                            
                                <?php 
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('post-thumbnails', array(
                                            'class' => 'img-fluid mb-3'
                                        ));
                                    }
                                ?>
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <?php endwhile; 
                        else : ?>
                            <div class="bf-card-title">
                                <h5 class="card-title"> No existe contenido </h5>
                            </div>    
                        <?php
                    endif;  wp_reset_postdata();  ?>
                    <!-- Paginación -->
                    <?php echo bootstrap_pagination($page_loop); ?> 
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>