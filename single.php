<?php get_header(); ?>
    <div class="container-fluid mb-4">
        <div class="row m-2">
            <!-- Entrada -->
            <div class="col-md-9 px-0">
                <div class="row title-container mx-0">
                    <div id="breadcrumb" class="col text-left px-2">
                            <a class="btn-breadcrumb" href="<?php echo get_home_url() ?>" name="volver atrás"><i class="fa fa-home"></i> Inicio </a>
                    </div>
                    <div id="breadcrumb" class="col text-right px-2">
                            <button class="btn-breadcrumb" onclick="history.back()" name="volver atrás"><i class="fa fa-undo"></i> atrás</button>
                    </div>
                </div>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="card">
                        <div class="card-body">
                            <div class="bf-card-title">
                                <h5 class="card-title"><?php the_title(); ?> </h5>
                            </div>                            
                            <?php 
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('post-thumbnails', array(
                                        'class' => 'img-fluid mb-3'
                                    ));
                                }
                            ?>
                            <?php the_content(); ?>
                            <p class="card-text text-right mb-0"><strong>Fecha de publicación : </strong><?php the_time('F j, Y'); ?>  </p>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-3">
                <!-- Sidebar Derecha -->
                <?php get_sidebar('right'); ?>  
            </div>
        </div>
    </div>
<?php get_footer();?>