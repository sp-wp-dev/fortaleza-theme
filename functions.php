<?php


if ( ! function_exists( 'fortaleza_setup' ) ) :

  function fortaleza_setup(){
    // Soporte imagenes destacadas
    date_default_timezone_set('UTC-4');

    $salar_installer = new Salar_Installer();
    $salar_installer->activate();
    // var_dump($salar_installer);
    if ( function_exists( 'add_theme_support' ) ) {
      add_theme_support( 'post-thumbnails' );
    }

    // hide adminBar
    if (!current_user_can('administrator') && !is_admin()) {
      show_admin_bar(false);
    }

    add_theme_support( 'title-tag' );

    // custom header Support
    $args = array(
      'default-image'		=>  get_template_directory_uri() .'/assets/images/header.png',
      'width'			=> '1600',
      'height'		=> '300',
      'flex-height'		=> true,
      'flex-width'		=> true,
      'header-text'		=> true,
      'default-text-color'	=> 'fff',
    );
    add_theme_support( 'custom-header', $args );


    //Custom logo
    add_theme_support(
      'custom-logo',
      array(
          'unlink-homepage-logo' => true, // Add Here!
        )
    );
    
    $args = array(
      'default-color' => '#eee',
      'default-image' => '',
    );
    add_theme_support( 'custom-background', $args );
  }
endif;
add_action( 'after_setup_theme', 'fortaleza_setup' );

//Agregar Sidebar
function fortaleza_widgets(){
  register_sidebar(array(
    'id' => 'sidebar-derecha',
    'name' => __('Sidebar Right'),
    'before_widget' => '<div id="%1$s" class="bf-widget %2$s">',
    'after_widget' => '</div></div>',
    'before_title' => '<div class="bf-wid-title"><h6>',
    'after_title' => '</h6></div><div class="bf-wid-content">',
    ));
   
   register_sidebar(array(
    'id' => 'sidebar-category',
    'name' => __('Sidebar Category'),
    'before_widget' => '<div id="%1$s" class="bf-widget %2$s">',
    'after_widget' => '</div></div>',
    'before_title' => '<div class="bf-wid-title"><h6>',
    'after_title' => '</h6></div><div class="bf-wid-content">',
    ));

  register_sidebar(array(
    'id' => 'sidebar-page',
    'name' => __('Sidebar Page'),
    'before_widget' => '<div id="%1$s" class="bf-widget %2$s">',
    'after_widget' => '</div></div>',
    'before_title' => '<div class="bf-wid-title"><h6>',
    'after_title' => '</h6></div><div class="bf-wid-content">',
    ));
}
                    
add_action('widgets_init','fortaleza_widgets');

//add attachment to search
function attachment_search( $query ) {
  if ( $query->is_search && !is_admin()) {
     $query->set( 'post_type', array( 'post', 'attachment' ) );
     $query->set( 'post_status', array( 'publish', 'inherit' ) );
  }

 return $query;
}

add_filter( 'pre_get_posts', 'attachment_search' );

function wpse_filter_child_cats( $query ) {

  if ( $query->is_category ) {
      $queried_object = get_queried_object();
      $child_cats = (array) get_term_children( $queried_object->term_id, 'category' );
  
      if ( ! $query->is_admin )
          //exclude the posts in child categories
          $query->set( 'category__not_in', array_merge( $child_cats ) );
      }
  
      return $query;
  }
  add_filter( 'pre_get_posts', 'wpse_filter_child_cats' );

//Registrar Menus
function fortaleza_register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Menú Superior' ),
     )
   );
 }
add_action( 'init', 'fortaleza_register_my_menus' );

/**
 * Filters the `sizes` value in the header image markup.
 *
 */
function fortaleza_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'fortaleza_header_image_tag', 10, 3 );



// Add a new interval of a week
// See http://codex.wordpress.org/Plugin_API/Filter_Reference/cron_schedules
function bf_cron_schedule( $schedules ) {
    $schedules['1min'] = array(
        'interval' => 60, 
        'display'  => __( 'Once every 1 minutes' ),
    );
    return $schedules;
}
add_filter( 'cron_schedules', 'bf_cron_schedule' );
 
// Schedule an action if it's not already scheduled
if ( ! wp_next_scheduled( 'bf_cron_action' ) ) {
    wp_schedule_event( time(), '1min', 'bf_cron_action' );
}
 
// Hook into that action that'll fire weekly
add_action( 'bf_cron_action', 'bf_function_to_run' );
function bf_function_to_run() {
  // error_log('Mi evento se ejecutó: '.Date("h:i:sa"));
  require_once get_template_directory() . '/inc/salar/class-salar.php';
  require_once get_template_directory() . '/inc/class-bf-birthday.php';
  require_once get_template_directory() . '/inc/salar/class-salar-install.php';

  $salar = new Salar();
  $salar->saveData();
  $salar->savePosition();

  $birthday = new Bf_birthday();
  $birthday->clearData();
  $birthday->setData();
}

add_filter( 'http_request_timeout', 'bf_timeout_extend' );

function bf_timeout_extend( $time ) {
    return 20;
}

if ( ! function_exists( 'bf_breadcrumbs' ) ) :
	/**
	 * Simple Breadcrumbs.
	 */
	function bf_breadcrumbs() {
		if ( ! function_exists( 'breadcrumb_trail' ) ) {
			require_once get_template_directory() . '/assets/library/breadcrumbs/breadcrumbs.php';
		}
		$args = array(
			'container'   => 'div',
			'show_browse' => false,
		);
		breadcrumb_trail($args);
	}

endif;
function bootstrap_pagination( \WP_Query $wp_query = null, $echo = true, $params = [] ) {
    
  if ( null === $wp_query ) {
      global $wp_query;
  }
  $add_args = [];

  $pages = paginate_links( array_merge( [
          'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
          'format'       => '?paged=%#%',
          'current'      => max( 1, get_query_var( 'paged' ) ),
          'total'        => $wp_query->max_num_pages,
          'type'         => 'array',
          'show_all'     => false,
          'end_size'     => 3,
          'mid_size'     => 1,
          'prev_next'    => true,
          'prev_text'    => __( '« Prev' ),
          'next_text'    => __( 'Next »' ),
          'add_args'     => $add_args,
          'add_fragment' => ''
      ], $params )
  );

  if ( is_array( $pages ) ) {
      $pagination = '<div class="pagination"><ul class="pagination">';
      foreach ( $pages as $page ) {
          $pagination .= '<li class="page-item '.(strpos($page, 'current') !== false ? 'active' : '').'"> ' . str_replace( 'page-numbers', 'page-link', $page ) . '</li>';
      }

      $pagination .= '</ul></div>';

      if ( $echo ) {
          echo $pagination;
      } else {
          return $pagination;
      }
  }

  return null;
}
//disable update plugin
function disable_plugin_updates( $value ) {
  unset( $value->response['my-calendar/my-calendar.php'] );
  unset( $value->response['birthdays-widget/birthdays-widget.php'] );
  return $value;
}
add_filter( 'site_transient_update_plugins', 'disable_plugin_updates' );

function create_category_by_page( $post_id, $post ) {

  if ($post->post_status =='publish') {
    if ( 'page' !== $post->post_type ) {
      return;
    }
    if ( $post->post_parent ==0){
      wp_insert_category(
        array(
          'cat_name'        => $post->post_title,
          'category_nicename'=>$post->post_title,//slug
          'category_parent' => 0,
        ));
    }else{
      $post_parent= get_post($post->post_parent);
      $slug_category='';
      while($post_parent->post_parent != 0){
        $slug_category.='-'.$post_parent->post_name;
        $post_parent = get_post($post_parent->post_parent);
      }
      $slug_category.='-'.$post_parent->post_name;
      $terms = get_terms( 'category', array(
        'orderby'    => 'parent',
        'hide_empty' => 0,
        'slug'    => $slug_category,
      ) );
      $category = get_terms( 'category', array(
        'orderby'    => 'parent',
        'hide_empty' => 0,
        'slug'    => $post->post_name.'-'.$slug_category,
      ) );
      if(count($category)==0){

        wp_insert_category(
          array(
            'cat_name'        => $post->post_title,
            'category_nicename'=>$post->post_name.'-'.$slug_category,
            'category_parent' => $terms[0]->term_id,
          ));
      }
      else{
        wp_insert_category(
          array(
            'cat_ID'          => $category[0]->term_id,
            'cat_name'        => $post->post_title,
            'category_nicename'=>$post->post_name.'-'.$slug_category,
            'category_parent' => $terms[0]->term_id,
          ));
      }
      
    }
  }
}
add_action( 'publish_page', 'create_category_by_page', 10,2 );

/**
 * Enqueue scripts and styles.
*/

function js_calendarios(){
  wp_enqueue_style('calendario-css', get_template_directory_uri().'/assets/library/datepicker/jquery-ui.css', array(), '1.0');
  wp_enqueue_style('calendario-css-2',  get_template_directory_uri().'/assets/library/datepicker/jquery-ui.theme.css', array(), '1.0');
  wp_enqueue_style('calendario-css-3',  get_template_directory_uri().'/assets/library/datepicker/jquery-ui.structure.css', array(), '1.0');
  wp_enqueue_script('calendario-js',  get_template_directory_uri().'/assets/library/datepicker/jquery-ui.js', array('jquery'), '1.0');
  wp_enqueue_script('app.js',get_template_directory_uri().'/assets/js/app-admin.js',array('jquery'),'1.0',true);
}
add_action('admin_enqueue_scripts', 'js_calendarios');

function add_theme_scripts() {
  
  wp_enqueue_style('style', get_stylesheet_uri());
  wp_enqueue_style( 'bootstrap-style', get_template_directory_uri().'/assets/library/bootstrap/css/bootstrap.min.css', array(), '4.0.0');
	wp_enqueue_style('font-awesome-style', get_template_directory_uri().'/assets/library/font-awesome/css/font-awesome.css');

  
	wp_register_style( 'fortaleza-google-fonts', '//fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,500i,700,700i');
	wp_enqueue_style( 'fortaleza-google-fonts' );

	wp_enqueue_script('popper-script', get_template_directory_uri().'/assets/library/bootstrap/js/popper.min.js', array('jquery'), '1.12.9', true);
	wp_enqueue_script('bootstrap-script', get_template_directory_uri().'/assets/library/bootstrap/js/bootstrap.min.js', array('jquery'), '4.0.0', true);

  //js Customizer
  wp_enqueue_script('app.js',get_template_directory_uri().'/assets/js/app.js',array('jquery'),'1.0',true);
  wp_enqueue_script('fortaleza-marquee-js', get_template_directory_uri() . '/assets/js/jquery.marquee.js' , array('jquery'));
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );



function add_admin_scripts(){
  wp_register_script('search-admin', get_template_directory_uri().'/assets/js/adminSearch.js');
  wp_enqueue_script('search-admin', false, array(), false, true);
  wp_register_script('categories-in-field', get_template_directory_uri().'/assets/js/categoriesInField.js');
  wp_enqueue_script('categories-in-field', false, array(), false, true);
}
add_action( 'admin_enqueue_scripts','add_admin_scripts');
//Register Custon Navigation Walker
require_once get_template_directory(). '/template-parts/navbar.php';
require get_template_directory() . '/inc/cpt.php';

/**
 * Carga de modulos
 */
require get_template_directory() . '/inc/login.php';
require get_template_directory() . '/inc/roles.php';
require get_template_directory() . '/inc/capabilities.php';
require get_template_directory() . '/inc/options.php';
require_once get_template_directory() . '/inc/salar/class-salar-install.php';
require_once get_template_directory() . '/inc/salar/class-salar.php';
require_once get_template_directory() . '/inc/class-bf-birthday.php';
require get_template_directory() . '/inc/widgets/widgets-init.php';

// Publicar servicio de la tabla wp_user_salar
// Function Callback
function rest_get_links_from_post($data) {
  global $wpdb;

  $table = $wpdb->base_prefix . 'wp_user_salar';
  $sql = "select * from view_directory";
  $option_id = $data['option_id'];

  $result = $wpdb->get_results($wpdb->prepare($sql));
  return $result;
}

// Uso del Hook ..
add_action('rest_api_init',function() {
  register_rest_route('user_salar/v1','/links/(?P<option_id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'rest_get_links_from_post',
  )); 
});

/**
 * Devuelve la categoría del padre superior que lo contiene de un post_type attachment
 */
function get_category_parent_by_attachment($post){
  $post_parent = get_post($post->post_parent);
  $categories = wp_get_post_categories($post_parent->ID);
  $category = get_category($categories[0]);
  while($category->category_parent!=0){
    $category = get_category($category->category_parent);
  }
  return $category;
}
function get_category_container_by_attachment($post){
  $post_parent = get_post($post->post_parent);
  $categories = wp_get_post_categories($post_parent->ID);
  $category = get_category($categories[0]);
  return $category;
}
/**
 * Devuelve la categoría del padre superior que lo contiene de un post
 */
function get_category_parent_by_post($post){
  $categories = wp_get_post_categories($post->ID);
  $category = get_category($categories[0]);
  while($category->category_parent!=0){
    $category = get_category($category->category_parent);
  }
  return $category;
}
function get_category_container_by_post($post){
  $post_parent = get_post($post->post_parent);
  $categories = wp_get_post_categories($post_parent->ID);
  $category = get_category($categories[0]);
  return $category;
}
/**
 * Devuelve la pagina padre superior de una pagina
 */
function get_page_parent_by_page($post){
  if($post->post_parent == 0){
    return $post;
  }
  $post_parent = get_post($post->post_parent);
  while ($post_parent->post_parent !== 0){
    $post_parent= get_post($post_parent->post_parent);
  }
  return $post_parent;
}