<?php get_header(); ?>
    <div class="bf-page">
        
        <div class="container-fluid mb-4">
            <div class="row m-2">
                <!-- Entrada -->
                <div class="col-md-9 px-0">
                    <div class="col title-container">
                        <h2 class="title-page">
                            <?php single_post_title(); ?>
                        </h2>
                    </div>
                    <?php if ( have_posts() ) : while (have_posts() ) : the_post(); ?>
                        <div class="card">
                            <div class="card-body">
                                <div class="bf-card-title">
                                    <h5 class="card-title"><?php the_title(); ?> </h5>
                                </div>                            
                                <?php 
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('post-thumbnails', array(
                                            'class' => 'img-fluid mb-3'
                                        ));
                                    }
                                ?>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    <?php endwhile; endif; ?>
                </div>
                <div class="col-md-3">
                <!-- Sidebar Derecha -->
                <?php get_sidebar('right'); ?>  
            </div>
            </div>
        </div>
    </div>
<?php get_footer();?>