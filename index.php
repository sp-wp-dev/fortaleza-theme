<?php get_header(); ?>

    <div class="container-fluid mb-4">
        <?php get_template_part('template-parts/featured','section'); ?>  
        <div class="row m-2">

            <div class="col-md-9 px-0">
                <!-- Categoria Legal -->
                <h3 class="title-category-index">Area Legal</h3>
                <?php echo do_shortcode('[pt_view id="10fda71gr0"]'); ?>
                <!-- Categoria Negocios -->
                <h3 class="title-category-index">Division  Negocios</h3>
                <?php echo do_shortcode('[pt_view id="c4894d0zqe"]'); ?>
                <!-- Categoria O&M -->
                <h3 class="title-category-index">Division Riesgos</h3>
                <?php echo do_shortcode('[pt_view id="0649573bk5"]'); ?>
                <!-- Categoria Operaciones -->
                <h3 class="title-category-index">Division Operaciones</h3>
                <?php echo do_shortcode('[pt_view id="a55b3f0021"]'); ?>
            </div>
            <!-- Entrada -->
            <div class="col-md-3">
               <!-- Sidebar Derecha -->
               <?php get_sidebar('right'); ?>  
            </div>
        </div>
    </div>
<?php get_footer();?>