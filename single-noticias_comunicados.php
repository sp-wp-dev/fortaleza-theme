<?php get_header(); ?>
    <div class="bf-page container-fluid mb-4">
        <div class="row my-2">
            <div class="col-md-9 px-0">
                <div class="row title-container mx-0">
                    <div class="col">
                        <h2 class="title-page">
                            <?php single_post_title(); ?>
                        </h2>
                    </div>
                    <div id="breadcrumb" class="col text-right pr-2">
                                <?php bf_breadcrumbs(); ?>
                    </div>
                </div>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="card">
                        <div class="row">
                            <div class="col-md-5">
                                <?php 
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('post-thumbnails', array(
                                            'class' => 'img-fluid mb-3'
                                        ));
                                    }
                                ?>
                            </div>
                            <div class="col-md-7">
                                 <div class="card-body ">
                                    <div class="bf-card-title">
                                        <h5 class="card-title"><?php the_title(); ?> </h5>
                                    </div>
                                    <?php the_content(); ?>
                                    <p class="card-text text-right mb-0"><strong>Fecha de publicación : </strong><?php the_time('F j, Y'); ?>  </p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-3">
                <!-- Sidebar Derecha -->
                <?php get_sidebar('right'); ?>  
            </div>
        </div>
    </div>
<?php get_footer();?>