        <!-- Footer -->
        <footer class="pb-1 bg-dark">
            <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; Banco Fortaleza 2020</p>
            </div>
            <!-- /.container -->
        </footer>

        <?php wp_footer(); ?>
    </body>
</html>